from django.apps import AppConfig


class GrammacolorappConfig(AppConfig):
    name = 'grammacolorapp'
