from django.shortcuts import render
from django.http import JsonResponse
import re, json, spacy, subprocess
import MeCab

print("Chargement du modèle de français...")
nlpFr = spacy.load('fr_core_news_md')
print("OK")
print("Chargement du modèle d'anglais...")
nlpEn = spacy.load('en_core_web_md')
print("OK")
print("Chargement du modèle de mandarin...")
nlpZh = spacy.load('zh_core_web_md')
print("OK")
print("Chargement du modèle de japonais...")
chasen = MeCab.Tagger("-Ochasen")
print("OK")


def home(request):
    updateTimeStr = updateTime()
    return render(request, 'coloriser.html', {'updateTime': updateTimeStr})

def runSpacy(request):
    colis = json.loads(request.body)
    text = colis['inText']
    lang = colis['lang']

    if lang == "fr":
        nlpText = nlpFr(text)
    elif lang == "en":
        nlpText = nlpEn(text)
    elif lang == "zh":
        nlpText = nlpZh(text)
    elif lang == "jp":
        nlpText = chasen.parse(text)

    outText = []

    if lang in ["fr","en"]:
        for token in nlpText:  
            tokenInfo = {
                "text": token.text,
                "lemma": token.lemma_,
                "pos": token.pos_,
                "tag": str(token.morph),
                "dep": token.dep_,
                "shape": token.shape_
            }
            print(tokenInfo)
            outText.append(tokenInfo)
            
            if token.whitespace_:
                outText.append({
                    "text": token.whitespace_,
                    "lemma": "",
                    "pos": "SPACE",
                    "tag": "SPACE",
                    "dep": "",
                    "shape": ""
                })
    elif lang in ["zh"]:
        for token in nlpText:  
            tokenInfo = {
                "text": token.text,
                "lemma": token.lemma_,
                "pos": token.pos_,
                "tag": token.tag_,
                "dep": token.dep_,
                "shape": token.shape_
            }
            print(tokenInfo)
            outText.append(tokenInfo)
            
            if token.whitespace_:
                outText.append({
                    "text": token.whitespace_,
                    "lemma": "",
                    "pos": "SPACE",
                    "tag": "SPACE",
                    "dep": "",
                    "shape": ""
                })
    elif lang == "jp":
        print(nlpText)
        nlpText = nlpText.split('\n')
        for token in nlpText:
            if token == "EOS":
                tokenInfo = {
                    "text": "\n",
                    "lemma": "",
                    "pos": "X",
                    "saibunrui1": "",
                    "saibunrui2": "",
                    "tag": "",
                    "dep": "",
                    "shape": ""
                }
            elif len(token) > 0:
                token = token.split('\t')
                tokenInfo = {
                    "text": token[0],
                    "lemma": token[2],
                    "pos": token[3].split('-')[0],
                    "saibunrui1": token[3].split('-')[1] if len(token[3].split('-'))>1 else "",
                    "saibunrui2": token[3].split('-')[2] if len(token[3].split('-'))>2 else "",
                    "tag": " ".join(token[3:]),
                    "dep": "",
                    "shape": ""
                }
            print(tokenInfo)
            outText.append(tokenInfo)
        
    rep = {
        'outText': outText
    }
    return JsonResponse(rep)

def updateTime():
    upd = str(subprocess.check_output(["git", "log", "-1", "--format=%cd", "--date=short"]))
    #ver = str(subprocess.check_output(["git", "rev-list", "--all", "--count"]))
    return 'Version 2.0 (mise à jour le '+upd[2:-3]+')'