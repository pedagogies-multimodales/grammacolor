var langJson = {
    "ti_titre": {
        "fr":"GrammaChrome - Cliquez ici pour en savoir plus !",
        "en":"GrammaChrome - Click here to know more!",
        "zh":"GrammaChrome - Click here to know more!",
        "jp":"GrammaChrome - 詳しくはこちら！"
    },
    "sp_header": {
        "fr":"Cette application est un prototype en cours de conception. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Accéder au forum</b></a> pour échanger avec la communauté et faire vos suggestions.",
        "en":"This application still is a prototype under development. <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>Access to the forum</b></a> to share and make suggestions.",
        "zh":"该应用是目前正在开发的原型。 <a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>进入社群论坛</b></a>交流，提出您的建议。",
        "jp":"このアプリは開発中です。コミュニティーでの交流や、改善のご提案は<a href='https://groups.google.com/d/forum/alem-app' target='_blank'><b>フォーラムへ</b></a>！"
    },
    "sp_footcode": {
        "fr":"Code source",
        "en":"Source code",
        "zh":"Source code",
        "jp":"ソースコード"
    },
    "sp_footjournal": {
        "fr":"Journal des modifications",
        "en":"Modifications log",
        "zh":"Modifications log",
        "jp":"編集ログ"
    },
    "sp_footlicence": {
        "fr":"Code open source sous licence CC BY-NC-SA 4.0",
        "en":"Open source code under CC BY-NC-SA 4.0 licence",
        "zh":"Open source code under CC BY-NC-SA 4.0 licence",
        "jp":"このサイトの内容物はクリエイティブ・コモンズ表示-非営利-継承4.0ライセンスの下に提供されています。"
    },
    "sp_footSpaCy": {
        "fr": "Analyse morphosyntaxique :",
        "en": "Morphosyntactic analysis:",
        "zh": "句法分析: ",
        "jp":"形態素解析: "
    },
    "sp_subtitle": {
        "fr": "<span style='color:#00b54a'>Coloriseur</span> <span style='color:#980086'>grammatical</span> <span style='color:#fff'>et</span> <span style='color:#980086'>automatique</span> <span style='color:#c00018'>de</span> <span style='color:#00b54a'>texte</span>",
        "en": '<span style="color:#eda01a;">Colourize</span><span> </span><span style="color:#a56026;" >your</span><span> </span><span style="color:#00b54a;" >texts</span><span> </span><span style="color:#c00018;" >with</span><span> </span><span style="color:#f3eb20;" >the</span><span> </span><span style="color:#00b54a;" >Grammar</span><span> </span><span style="color:#c00018;" >in</span><span> </span><span style="color:#00b54a;" >Colours</span>',
        "zh": "<span class='token' style='color:#980086'>自動</span><span class='token' style='color:#00b54a'>语法</span><span class='token' style='color:#fff'>和</span><span class='token' style='color:#00b54a'>文本着色器</span>",
        "jp":'<span class="token" style="color: #00b54a">文章</span><span class="token" style="color: #00b54a">自動</span><span class="token" style="color: #00b54a">文法</span><span class="token" style="color: #00b54a">解析</span><span class="token" style="color: #00b54a">器</span>'
    },
    "sp_consigne": {
        "fr": "Entrez du texte à analyser :",
        "en": "Enter some text to colourize:",
        "zh": "请输入一段文字：",
        "jp":"解析したい文章を入力してください："
    },
    "sp_exemple": {
        "fr": "Générer un exemple",
        "en": "Give me an example",
        "zh": "提出一个例子",
        "jp":"例文を表示する"
    },
    "sp_resultat": {
        "fr": "Résultats :",
        "en": "Output:",
        "zh": "结果:",
        "jp":"結果："
    },
    "sp_tuto": {
        "fr": "Accéder aux tutoriels",
        "en": "Access to our tutorials",
        "zh": "访问教程",
        "jp": "チュートリアルにアクセス"
    },
    "ti_btntags": {
        "fr": "Afficher/masquer les catégories au survol de la souris",
        "en": "Show/hide grammatical categories on mouse hover",
        "zh": "Show/hide grammatical categories on mouse hover",
        "jp":"カーソルを合わせて品詞を表示"
    },
    "ti_btnCopierColler": {
        "fr": "Copier/Coller le contenu",
        "en": "Copy/paste content",
        "en": "Copy/paste content",
        "jp": "結果をコピーする",
        "zh": "复制结果"
    },
    "ti_btnCouleurFond": {
        "fr": "Changer la couleur du fond",
        "en": "Switch background color",
        "en": "Switch background color",
        "zh": "切换背景色",
        "jp": "背景色の変更"
    },
    "ti_btnwordspace": {
        "fr": "Espacer les mots",
        "en": "Space between words",
        "zh": "单词之间放置空格",
        "jp": "単語間に空白を入れる"
    },
    "sp_analyser": {
        "fr": "Analyser",
        "en": "Analyze",
        "zh": "分析",
        "jp":"解析"
    },
    "sp_effacer": {
        "fr": "Effacer",
        "en": "Erase",
        "zh": "重置",
        "jp":"削除"
    },
    "sp_info": {
        "fr": "La colorisation de GrammaChrome se base sur une analyse syntaxique statistique qui \"prédit\" la catégorie la plus probable en fonction du contexte et des règles syntaxiques de la langue cible. Il arrive qu'il y ait des erreurs. L'analyse est faite par <a href='https://spacy.io/'>SpaCy</a> pour le français, l'anglais et pour le mandarin. La colorisation du japonais recourt à l'analyseur morphosyntaxique <a href='https://taku910.github.io/mecab/'>MeCab</a> et au dictionnaire <a href='https://salsa.debian.org/nlp-ja-team/mecab-ipadic'>IPADIC</a>.",
        "en": "La colorisation de GrammaChrome se base sur une analyse syntaxique statistique qui \"prédit\" la catégorie la plus probable en fonction du contexte et des règles syntaxiques de la langue cible. Il arrive qu'il y ait des erreurs. L'analyse est faite par <a href='https://spacy.io/'>SpaCy</a> pour le français, l'anglais et pour le mandarin. La colorisation du japonais recourt à l'analyseur morphosyntaxique <a href='https://taku910.github.io/mecab/'>MeCab</a> et au dictionnaire <a href='https://salsa.debian.org/nlp-ja-team/mecab-ipadic'>IPADIC</a>.",
        "zh": "La colorisation de GrammaChrome se base sur une analyse syntaxique statistique qui \"prédit\" la catégorie la plus probable en fonction du contexte et des règles syntaxiques de la langue cible. Il arrive qu'il y ait des erreurs. L'analyse est faite par <a href='https://spacy.io/'>SpaCy</a> pour le français, l'anglais et pour le mandarin. La colorisation du japonais recourt à l'analyseur morphosyntaxique <a href='https://taku910.github.io/mecab/'>MeCab</a> et au dictionnaire <a href='https://salsa.debian.org/nlp-ja-team/mecab-ipadic'>IPADIC</a>.",
        "jp": "GrammaChromeでは<a href='https://taku910.github.io/mecab/'>MeCab</a>と<a href='https://salsa.debian.org/nlp-ja-team/mecab-ipadic'>IPADIC</a>の形態素解析を利用して色分けをしています。フランス語版、英語版と中国語版は<a href='https://spacy.io/'>SpaCy</a>を利用しています。"
    },
    "ti_btnCustom": {
        "fr": "Personnaliser la colorisation",
        "en": "Customise the output",
        "zh": "Customise the output",
        "jp": "Customise the output"
    },
    "sp_customExplain": {
        "fr": "Choisissez les catégories à coloriser et leur couleur",
        "en": "Select the parts of speech to colourize and set their colour",
        "zh": "Select the parts of speech to colourize and set their colour",
        "jp": "Select the parts of speech to colourize and set their colour"
    },
    "sp_customColBase": {
        "fr": "Couleur de base :",
        "en": "Base color:",
        "zh": "Base color:",
        "jp": "Base color:"
    }
}