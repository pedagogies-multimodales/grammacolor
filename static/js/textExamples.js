var textExamples = {
    "fr": [
        "L'apprentissage consiste à acquérir ou à modifier une représentation d'un environnement de façon à permettre avec celui-ci des interactions ou des relations efficaces ou de plus en plus efficaces.",
        "Le dolmen de Pierre-Fade se dresse sur les flancs d'un coteau, plus précisément sur une rupture de pente, à 674 m d'altitude, au lieu-dit Les Brousses, sur la commune de Saint-Étienne-des-Champs (parcelle cadastrale AX n°131).\n En contrebas, coule la rivière du Sioulet, ce qui aurait pu être un facteur déterminant pour l'installation du monument. En effet de nombreux galets ont été retrouvés dans la structure du tumulus (galets en basalte) et dans la couche archéologique de la chambre (petits galets de quartz et de micaschiste).",
        "La licorne est une figure héraldique imaginaire féminine qui, selon la tradition, ressemble au cheval, a des sabots fourchus de cervidé, une barbiche sous la gueule et parfois une queue de lion.\n On la trouve surtout dans les ornements extérieurs de l'écu.",
        "Le Monde et des tiers selectionnés, notamment des partenaires publicitaires, utilisent des cookies ou des technologies similaires.\n Les cookies nous permettent d’accéder à, d’analyser et de stocker des informations telles que les caractéristiques de votre terminal ainsi que certaines données personnelles (par exemple : adresses IP, données de navigation, d’utilisation ou de géolocalisation, identifiants uniques).",
        "La fête de leurs 18 ans se joue à la roulette russe. Né en avril ? Confinement, on oublie les réjouissances… Né en juillet ? Déconfinement, fiesta ! Fin octobre ? Couvre-feu, au lit avec les poules.\n Novembre ? Reconfinement, pas de bol… Pour qui est né en 2002, la célébration, tant attendue, de la majorité dans l’excitation et les effluves d’alcool tient du coup de chance.",
        "Rarement pareille guerre de communication n’aura animé une campagne précédant une énième « votation » au sein de la Confédération helvétique.\n Banderoles orange des partisans sur les balcons des appartements, pleines pages de publicité des opposants dans les grands quotidiens de Zurich et Genève, saturation des réseaux sociaux, et spam massif des messageries électroniques : dimanche 29 novembre, les citoyens suisses devront dire s’ils acceptent une « initiative pour des multinationales responsables », soutenue par la gauche et combattue par le patronat et les principaux partis de droite, à l’exception du PDC (démocrate-chrétien), divisé sur la question.",
        "Les Français ont découvert les mouchoirs en papier grâce aux Japonais, en 1615 à Saint-Tropez.",
        "L’invention de la choucroute est souvent attribuée aux bâtisseurs de la Grande Muraille de Chine, et son introduction en Europe aux Huns d’Attila."
    ],
    "en": [
        "Adjectives are words that typically modify nouns and specify their properties or attributes:\n\"The oldest French bridge\"\nThey may also function as predicates, as in:\n\"The car is green.\"",
        "NASA Astronaut Group 2 was the second group of astronauts selected by NASA; their names were announced on September 17, 1962. President Kennedy had announced Project Apollo, with the goal of putting a man on the Moon by the end of the decade, and more astronauts were required to fly the two-person Gemini spacecraft and three-person Apollo spacecraft then under development.",
        "The science of color is sometimes called chromatics, colorimetry, or simply color science. It includes the perception of color by the human eye and brain, the origin of color in materials, color theory in art, and the physics of electromagnetic radiation in the visible range (that is, what is commonly referred to simply as light).",
        "In linguistics, the grammar of a natural language is its set of structural constraints on speakers' or writers' composition of clauses, phrases, and words. The term can also refer to the study of such constraints, a field that includes domains such as phonology, morphology, and syntax, often complemented by phonetics, semantics, and pragmatics. There are currently two different approaches to the study of grammar: traditional grammar and theoretical grammar.",
        "Twas brillig, and the slithy toves\nDid gyre and gimble in the wabe;\nAll mimsy were the borogroves,\nAnd the mome rathes outgrabe.",
        "It was cold, and the little fish twisted and tumbled in the water; the birds were all quiet, and the proud lions roared."
    ],
    "zh": [
        "在古埃及，中国和所有其他的文明中都留下很多文字。\n在中世纪的末期，因为封建领主想占据法理上的优势，推动了对古代文本的研究。",
        "日本自第三十六代孝德天皇即位並建元大化起開始使用年号為其纪年方式，並在文武天皇於701年5月3日（大寶元年三月二十一日）再度恢復使用年號後，一直持續使用年號至今。\n光嚴天皇為日本南北朝時代中由持明院統系擔任天皇的北朝的首任天皇，受镰仓幕府擁立。他在1332年5月23日（元弘二年四月二十八日）改元正慶，是為北朝年號之始，惟光嚴天皇在1333年7月7日（正慶二年／元弘三年五月二十五日）被後醍醐天皇廢位。光嚴天皇之弟光明天皇在後醍醐天皇的建武新政失敗後受足利氏擁立繼位為北朝天皇，並在1338年10月11日（建武五年／延元三年八月二十八日）改元曆應。",
        "紫阳书院，是清代位于苏州的一所著名书院，“紫阳”的名字出自著名理学家的朱熹的表字。\n当时，官办学校一律以科举考试为教学内容，而紫阳书院坚持主要讲授朱熹理学，辅以有关科举考试的内容。张伯行还聘请崇明县教谕郭正宗、吴江县教谕夏声等人任教，一时汇集江南甚至全国各地的学生。",
        "特色内容是维基百科社群推荐的典范之作。展示在这里的条目、列表及图片，都是参与者在维基百科的精神感召之下共同协作努力的成果。",
        "臺灣島為板塊碰撞隆起形成的大陸島，是東亞島弧之一部分（由菲律賓板塊潛入歐亞板塊形成），約三萬年前冰河時期開始有人類遷移至台灣活動，自古為原住民族世居之地，原住民族在17世紀中葉以前一直居於主體民族地位；隨著漢族不斷從中國大陸移入與墾殖、以及與平埔族原住民通婚，漢族遂取代原住民族成為臺灣的最大民族。",
        "臺灣同性婚姻的相關議題與社會運動，起始於1980年代末期祁家威提出同性婚姻立法的請願與抗爭。臺灣婚姻規範的法源為中華民國《民法》親屬編，當中沒有保障同性婚姻或民事結合的法律地位。為了使同性婚姻在中華民國法制化，同志團體自2012年起積極推動《多元成家立法草案》，並就現行《民法》條文不承認同性婚姻提請司法院大法官釋憲。"
    ],
    "jp": [
        "若手ダンサーの登竜門、第４９回ローザンヌ国際バレエコンクールの決選が６日（現地時間）、スイスで行われ、埼玉県出身で米国のバレエ学校に在籍している淵山隼平さん（１８）が５位に入賞した。コロナ禍のため、ビデオ審査だった。",
        "日本語を母語としない方のための日本語教育の世界でも，「コーパス」という言葉を耳にすることが多くなってきました。まず，コーパスとは何か，そしてそのメリットについて確認しておきましょう。コーパスとは，「実際に使われた言語のデータを大量に集めた電子的なデータベース」のことです。そして，そのデータを利用するメリットとしては，「実際によく使われるパターンを見つけ出す」ことができる点にあります。",
        "不動産屋：こんにちは。\n客：お忙しい中、時間を割いていただきましてありがとうございます。\n不動産屋：いやいや、気にしないで下さい。じゃあ、１つ目の物件に行ってみましょうか。\n客：はい、お願いします。\n\n不動産屋：ここが、６万５千円のアパートです。\n客：え、写真と少し違いませんか？あっゴキブリ！ちょっとここは・・・\n不動産屋：まあ、駅から近いからとても便利ですけどね。\n客：近いのはいいですけど、線路の隣だと少しうるさくないですか。\n不動産屋：防音はしっかりしていますから、あまり気にならないと思います。",
        "アキ：もしよろしければ、英語で話してもいいですか。\nとも：もちろん、問題ありません。私も英語を練習しなければいけません。英語は、どれくらい長く勉強してますか。\nアキ：英語は、2年ぐらい勉強しています。でも、まだ話すのが大変です。",
        "人魚は、人間と魚類の特徴を兼ね備え、水中に生息するとされる伝説の生物。世界各地に類似の伝承があり、西洋のマーメイドなど個々の名称を持つこともあるが、伝承されてきた土地によりその形状や性質は大きく異なる。",
        "「日本」という漢字による国号の表記は、日本列島が中国大陸から見て東の果て、つまり「日の本（ひのもと）」に位置することに由来するという説がある。近代の二つの憲法の表題は、「日本国憲法」および「大日本帝国憲法」であるが、国号を直接かつ明確に規定した法令は存在しない。ただし、日本工業規格では「日本国」、英語表記をJapanと規定。更に、国際規格（ISO）では3文字略号をJPN、2文字略号をJPと規定している。"
    ]
}