// Liste des exceptions: écraser le résultat de Spacy par celui-ci.

var text2postag = {
    "fr": {
        "bonjour" : {"pos":"INTJ", "tag":""},
        "bravo" : {"pos":"INTJ", "tag":""},
        "aïe" : {"pos":"INTJ", "tag":""},
        "ah" : {"pos":"INTJ", "tag":""},
        "aah" : {"pos":"INTJ", "tag":""},
        "aha" : {"pos":"INTJ", "tag":""},
        "oh" : {"pos":"INTJ", "tag":""},
        "zut" : {"pos":"INTJ", "tag":""},

    }
}

// Écraser si mot seul dans la phrase :
// Ciel, Salut...