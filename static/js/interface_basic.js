const thisURL = window.location.href;
console.log("url:",thisURL);

var thisPageLang = "";

setLangFromUrl()
// set page target language
function setLangFromUrl() {
    var pageLang = thisURL.match(/.*\/(fr|en|zh|jp)/);
    if (pageLang) { 
        console.log("Langue indiquée par l'url:",pageLang[1]);
        thisPageLang = pageLang[1];
        if (pageLang[1]=="fr") selectLang("fr");
        if (pageLang[1]=="en") selectLang("en");
        if (pageLang[1]=="zh") selectLang("zh");
        if (pageLang[1]=="jp") selectLang("jp");
    } else { 
        console.log("Chargement langue par défaut (fr)");
        thisPageLang = "fr"
        selectLang("fr");
    }
}


function selectLang(lang){
    console.log('SelectLang()',lang);
    document.getElementById('choixLang').value = lang;
    interface(lang);
    window.history.pushState("", "", "/"+lang);
}

function interface(lang) {
    console.log("Langue d'interface:",lang);
    thisPageLang = lang;
	var langspanList = document.getElementsByClassName("langspan");
	var langtitleList = document.getElementsByClassName("langtitle");

    for (i=0; i<langspanList.length; i++) {
        span = langspanList[i];
        span.innerHTML = langJson[span.id][lang];
    };
    for (i=0; i<langtitleList.length; i++) {
        title = langtitleList[i];
        title.title = langJson[title.id][lang];
    };

    if (lang == 'fr') {
        document.getElementById('posTable').innerHTML = posTableFr;
        makeCustom();
    } 
    if (lang == 'en') {
        document.getElementById('posTable').innerHTML = posTableEn;
        makeCustom();
    } 
    if (lang == 'zh') {
        document.getElementById('posTable').innerHTML = posTableZh;
        makeCustom();
    } 
    if (lang == 'jp') {
        document.getElementById('posTable').innerHTML = posTableJp;
        makeCustom();
    } 
}