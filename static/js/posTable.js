var posTableFr = `
    <div class="d-flex justify-content-evenly">
        <!-- POS -->
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-pos')" id="checkAll" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="PRON"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="PRONADV"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="ADV"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="INTJ"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="ADJ"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="VERB"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="NOUN"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="PROPN"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="AUX"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="DET"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="NUM"></div></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="PREPDET"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="CCONJ"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="SCONJ"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="ADP"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="PUNCT"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="SYM"></div></td>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
`

var posTableEn = `
    <div class="d-flex justify-content-evenly">
        <!-- POS -->
        <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
            <div class="form-check align-self-center">
                <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-pos')" id="checkAll" checked>
            </div>
            <table style="background-color:white;">
                <tbody>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="PRON"></div></td>
                        <td></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="ADV"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="INTJ"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="ADJ"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="VERB"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="NOUN"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="PROPN"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="AUX"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="DET"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="NUM"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="PART"></div></td>
                        </tr>
                        <tr>
                        <td></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="CCONJ"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="SCONJ"></div></td>
                    </tr>
                    <tr>
                        <td class="phonTableTd"><div class="custcol" data-pos="ADP"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="PUNCT"></div></td>
                        <td class="phonTableTd"><div class="custcol" data-pos="SYM"></div></td>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
`

var posTableZh = `
<div class="d-flex justify-content-evenly">
    <!-- POS -->
    <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
        <div class="form-check align-self-center">
            <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-pos')" id="checkAll" checked>
        </div>
        <table style="background-color:white;">
            <tbody>
                <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="PN"></div></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="AD"></div></td>
               </tr>
               <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="IJ"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="ON"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="VA"></div></td>
                    <td class="phonTableTd"></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="NN"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="NR"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="VV"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="VC"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="NT"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="FW"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="VE"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="AS"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="DT"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="CD"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="OD"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="M"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="LC"></div></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="CC"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="CS"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="P"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="LB"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="SB"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="BA"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="DEC"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="DER"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="DEG"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="DEV"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="SP"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="MSP"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="JJ"></div></td>
                    <td class="phonTableTd"><div class="custcol" data-pos="ETC"></div></td>
                </tr>
                <tr>
                    <td class="phonTableTd"><div class="custcol" data-pos="PU"></div></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                    <td class="phonTableTd"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
`


var posTableJp = `
<div class="d-flex justify-content-evenly">
    <!-- POS -->
    <div class="d-flex p-1 m-1" style="background-color:#caf2ca;border:1px solid black; border-radius:5px">
        <div class="form-check align-self-center">
            <input class="form-check-input" type="checkbox" onchange="checkUncheck(this.checked,'check-pos')" id="checkAll" checked>
        </div>
        <table style="background-color:white;">
            <tbody>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・代名詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="副詞・一般"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="副詞・助詞類接続"></div></td>
                <td class="phonTableTd"></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="感動詞・"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="その他・間投"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="フィラー・"></div></td>
                <td class="phonTableTd"></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="形容詞・自立"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="形容詞・非自立"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="連体詞・"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="形容詞・接尾"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="連体詞・係助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="連体詞・副助詞／並立助詞／終助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="連体詞・特殊"></div></td>
                <td class="phonTableTd"></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・一般"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・固有名詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・引用文字列"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・動詞非自立的"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・非自立"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・形容動詞語幹"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・接尾"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・特殊"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・数"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・接続詞的"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・サ変接続"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・ナイ形容詞語幹"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="接頭詞・名詞接続"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="接頭詞・数接続"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="名詞・副詞可能"></div></td>
                <td class="phonTableTd"></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・副助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・接続助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・格助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・連体化"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・係助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・副助詞／並立助詞／終助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・副詞化"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・並立助詞"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="助詞・終助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="動詞・自立"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="助動詞・"></div></td>
                <td class="phonTableTd"></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="接頭詞・形容詞接続"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="動詞・非自立"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="接頭詞・動詞接続"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="動詞・接尾"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="接続詞・"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="接続詞・並立助詞"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="接続詞・副詞化"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="接続詞・終助詞"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="記号・アルファベット"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="記号・一般"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="記号・句点"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="記号・括弧閉"></div></td>
                </tr>
                <tr>
                <td class="phonTableTd"><div class="custcol" data-pos="記号・括弧開"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="記号・空白"></div></td>
                <td class="phonTableTd"><div class="custcol" data-pos="記号・読点"></div></td>
                <td class="phonTableTd"></td>
                </tr>
            </tbody>
        </table>
        </div>
        </div>
        <p>Tableau à restructurer et simplifier. Plus d'infos ici : <a href='https://alem.hypotheses.org/464'>https://alem.hypotheses.org/464</a></p>
`