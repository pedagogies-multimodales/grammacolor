function runIfSpace() {
	var inText = document.getElementById('inText').innerText;
	console.log("Typing...");
	if (inText.slice(-1)==" " || inText.slice(-1)=="\n") {
		runSpacy();
		console.log("runSpacy()");
	}
}

async function runSpacy() {
    addStat("coloriser",thisPageLang);

	document.getElementById('outText').innerHTML = "";
	document.getElementById('loader').style.display = "block";

	var inText = document.getElementById('inText').value;
	var lang = document.getElementById('choixLang').value;
    console.log(lang)

	// REMPLACEMENT DES \n PAR ¬ POUR MECAB (JP)
	if(lang == "jp") inText = inText.replace(/\n/g,'¬')

	// ON EMBALLE TOUT ÇA
    var colis = {
		inText: inText,
		lang: lang
	}
	console.log('Envoi colis:',colis);

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // REQUÊTAGE SERVEUR
	const response = await fetch('/runSpacy/', options)
	console.log('En attente de réponse...');
    const data = await response.json();
	console.log(data);

	// AFFICHAGE DU RÉSULTAT
	var result = "";

	var stylesCommuns = "font-family: Palatino, Times, KaiTi, Calibri; font-size: 35pt; font-weight: bold;";
	var stylesUnderline = "text-decoration: underline; text-decoration-thickness: 6px;";

	for (i=0; i<data.outText.length; i++) {
        var thisText = data.outText[i].text;
        var thisPos = data.outText[i].pos;
        var thisTag = data.outText[i].tag;
        console.log(thisText, thisPos, thisTag)
        if (Object.keys(text2postag).includes(lang) && Object.keys(text2postag[lang]).includes(thisText.toLowerCase())) {
            thisPos = text2postag[lang][thisText.toLowerCase()].pos;
            thisTag = text2postag[lang][thisText.toLowerCase()].tag;
        }

		if (lang=="jp") {
            if (Object.keys(pos).includes(thisPos + '・' + data.outText[i].saibunrui1)) col = pos[thisPos + '・' + data.outText[i].saibunrui1].color;
            else {
                col = "#cccccc"
                console.log("MANQUE:",thisPos + '・' + data.outText[i].saibunrui1)
            }
        } else if (lang=="zh") {
            if (Object.keys(pos).includes(thisTag)) col = pos[thisTag].color;
            else {
                col = "#cccccc"
                console.log("MANQUE:",thisTag + ' ' + thisPos)
            }
        } else {
            // console.log(thisPos)
            // console.log(pos[thisPos])
            col = pos[thisPos].color;
        }

        if (lang=="jp") var currentPos = thisPos + '・' + data.outText[i].saibunrui1;
        else if (lang=="zh") var currentPos = thisTag;
        else var currentPos = thisPos;

        // Traitement des tags
        var tagdef = "";
        

        if (lang == "fr" || lang == "en") {
            var taglist = Array.from(thisTag.split('|'))
            var tags = [];
            taglist.forEach((tag) => {
                if (tag in tags2def[lang]){
                    tags.push(tags2def[lang][tag].toLowerCase())
                } else if (tag!="") {
                    tags.push(tag)
                }
            })
            if (thisTag == thisPos) {
                tagdef = pos[thisPos][lang]
            } else if (tags.length>0) {
                tagdef = pos[thisPos][lang] + ' : ' + tags.join(', ')
            } else {
                tagdef = pos[thisPos][lang]
            }
        
        } else if (lang == "zh") {
            if (Object.keys(pos).includes(thisTag)) tagdef = pos[thisTag][lang]
            else tagdef = thisTag;
        
        } else if (lang == "jp") {
            // console.log(thisPos + '・' + data.outText[i].saibunrui1)
            // if (Object.keys(pos).includes(thisPos + '・' + data.outText[i].saibunrui1)) tagdef = pos[thisPos + ' ' + data.outText[i].saibunrui1].def;
            // else tagdef = thisPos + ' ' + data.outText[i].saibunrui1
            tagdef = thisPos + ' ' + data.outText[i].saibunrui1 + ' ' + data.outText[i].saibunrui2;
        }

		// saut de ligne
		sdl = thisText.split(/[\n¬]+/);
		if (sdl.length>1) {
			for (s=1; s<sdl.length; s++) {
				result = result + "<br>";
			}
		} else if (thisText == " ") {
            // ESPACE
            result = result 
                + "<span style='"+ stylesCommuns +"'> </span>";
        } else if ((thisText.toLowerCase() == "en" || thisText.toLowerCase() == "y") && thisPos == "PRON") {
            // BICOLORES → soulignage
            // pronoms adverbiaux (en et y)
            thisPos = currentPos = "PRONADV";
            tagdef = pos[thisPos][lang]
			result = result
				+ '<span style="color:'+ pos["PRON"].color +';'
				+ stylesCommuns
				+ stylesUnderline + 'text-decoration-color:' + pos["ADV"].color +'" '
                +'title-data="'+ tagdef +'" '
                +'class="token pos'+ currentPos +'" '
                +'>'+ thisText + '</span>';
		} else if (thisPos== "ADP" && ["du","au","aux"].includes(thisText)) {
            // BICOLORES → soulignage
            // déterminants prépositionnels (du des...)
            thisPos = currentPos = "PREPDET";
            tagdef = pos[thisPos][lang]
			result = result
				+ '<span style="color:'+ pos["DET"].color +';'
				+ stylesCommuns
				+ stylesUnderline + 'text-decoration-color:' + pos["ADP"].color +'" '
                +'title-data="'+ tagdef +'" '
                +'class="token pos'+ currentPos +'" '
                +'>'+ thisText + '</span>';
		} else {
		    // tout le reste
            result = result 
                + '<span style="color:'+ col +';'
                + stylesCommuns +'" '
                +'title-data="'+ tagdef +'" '
                +'class="token pos'+ currentPos +'" '
                +'>'+ thisText + '</span>';
        }
	}

	document.getElementById('loader').style.display = "none";
	document.getElementById('outText').innerHTML = result;

	// On relance switchTags() si il était actif
    switchTags(); 
    switchTags();

    // On remplace les blancs par noir si bg = blanc
    if (defaultBg == 'white') {
        document.getElementById('outText').innerHTML = document.getElementById('outText').innerHTML.replace(/#fffff;/g,'#000000;')
    }
}

function getExample() {
	var lang = document.getElementById('choixLang').value;
	var inText = document.getElementById('inText');

	example = textExamples[lang][Math.floor(Math.random() * textExamples[lang].length)];
	inText.value = example;
	
}

function erase(){
	document.getElementById('inText').value = '';
	document.getElementById('loader').style.display = "none";
	document.getElementById('outText').innerHTML = "";
}


var defaultBg = "black";
function bg2white() {
	if (defaultBg == 'black') {
		defaultBg = 'white';
        document.getElementById('outText').innerHTML = document.getElementById('outText').innerHTML.replace(/#fff;/g,'#000;')
    } else {
		defaultBg = 'black';
		document.getElementById('outText').innerHTML = document.getElementById('outText').innerHTML.replace(/#000;/g,'#fff;')
	}
	document.getElementById('outputDiv').classList.toggle('win-white');
	document.getElementById('outText').classList.toggle('contours');
}


var tags = false;
switchTags();
function switchTags(){
	divBtnTags = document.getElementById('ti_btntags');
	tokens = document.getElementsByClassName('token');
	
	if (tags) { // Masque les étiquettes morphosyntaxiques
		for (i=0; i<tokens.length; i++){
			tokens[i].title = '';
			tokens[i].classList.add("token");
		}
		divBtnTags.classList.remove("btnActive");

	} else { // Affiche les étiquettes morphosyntaxiques
		for (i=0; i<tokens.length; i++){
			tokens[i].title = tokens[i].getAttribute('title-data');
			tokens[i].classList.add("token", "tokenborder");
		}
		divBtnTags.classList.add("btnActive");
	}
	tags = !tags;
}


var displaywordspace = false;
function togglewordspace(){
	divBtnSpace = document.getElementById('ti_btnwordspace');

	if (displaywordspace){ // Masque les furigana
		document.documentElement.style.setProperty('--tokenmargin', '20px 0px');
		displaywordspace = false;
		divBtnSpace.classList.remove("btnActive");

	} else { // Affiche les furigana
		document.documentElement.style.setProperty('--tokenmargin', '20px 15px');
		displaywordspace = true;
		divBtnSpace.classList.add("btnActive");
	}
}


//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
////////// POUR COPIER TEXTE /////////
//////////////////////////////////////
// Get the modal
var modal = document.getElementById("myModal");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Fonction pour ouvrir le PopUp 
function getPopUp() {
    var outputContent = document.getElementById('outText').innerHTML;
    
    var finalOutput = outputContent.replace(/#000;/g,'#fff;').replace(/ class="token \w+ tokenborder"/g, '').replace(/ title(-data)?=".*?"/g, '')

    var finalOutputWhite = outputContent.replace(/#fff;/g,'#000;').replace(/ class="token \w+ tokenborder"/g, '').replace(/ title(-data)?=".*?"/g, '')
	
	// Insertion des résultats dans les div associées du popup
	document.getElementById('pLienDiv').innerHTML = finalOutput;
    document.getElementById('pLienDivCode').innerHTML = finalOutput.toString();
	document.getElementById('pLienDivWhite').innerHTML = finalOutputWhite;
    document.getElementById('pLienDivWhiteCode').innerHTML = finalOutputWhite.toString();

	// Affichage popup
    modal.style.display = "block";
}


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
	}
}

// COPIE AUTO DU LIEN DANS PRESSE-PAPIER
function toClipBoard(containerid,tooltipid) {
    //var copyText = document.getElementById("pLienDiv");
    // copyText.select();
    // copyText.setSelectionRange(0, 99999);
	// document.execCommand("copy");

	if (document.selection) {
		var range = document.body.createTextRange();
		range.moveToElementText(document.getElementById(containerid));
		range.select().createTextRange();
		document.execCommand("copy");
	} else if (window.getSelection) {
		var range = document.createRange();
		range.selectNode(document.getElementById(containerid));
		window.getSelection().empty();
		window.getSelection().addRange(range);
		document.execCommand("copy");
	}
	
    
	var tooltip = document.getElementById(tooltipid) ;
	tooltip.innerHTML = "Copié !";
}
    
function outFunc(tooltipid) {
    var tooltip = document.getElementById(tooltipid);
    tooltip.innerHTML = "Copier";
}