function setCol(thispos, color) {
    pos[thispos].color = color;
    if (thispos == "col_base") {
        var listunset = document.querySelectorAll('.unset');
        for (i=0; i<listunset.length; i++) {
            listunset[i].style.color = color;
        }
    } else {
        var listpos = document.querySelectorAll('.pos'+thispos);
        for (i=0; i<listpos.length; i++) {
            listpos[i].style.color = color;
        }
    }

    console.log("Set color of",thispos,"to",color);
}


function togglePhon(cbid) {
    var thisCb = document.getElementById(cbid);
    var thisPos = cbid.slice(3);
    console.log(thisCb.checked, thisPos);
    var listthispos = document.querySelectorAll('.pos'+thisPos);
    if (thisCb.checked) {
        if (!bi.includes(thisPos)) pos[thisPos].color = document.getElementById('colorPicker-'+thisPos).value;
        for (i=0; i<listthispos.length; i++) {
            if (!bi.includes(thisPos)) {
                listthispos[i].style.color = document.getElementById('colorPicker-'+thisPos).value;
            } else if (thisPos=="PREPDET") {
                listthispos[i].style.color = document.getElementById('colorPicker-DET').value;
                listthispos[i].style.textDecorationColor = document.getElementById('colorPicker-ADP').value;
            } else if (thisPos=="PRONADV") {
                listthispos[i].style.color = document.getElementById('colorPicker-PRON').value;
                listthispos[i].style.textDecorationColor = document.getElementById('colorPicker-ADV').value;
            }
            listthispos[i].classList.remove('unset');
        }
    } else {
        pos[thisPos].color = pos["col_base"].color;
        for (i=0; i<listthispos.length; i++) {
            listthispos[i].style.color = pos["col_base"].color;
            if (bi.includes(thisPos)) listthispos[i].style.textDecorationColor = pos["col_base"].color;
            listthispos[i].classList.add('unset');
        }
    }
}

function checkUncheck(thisChecked, classe) {
    var cblist = document.querySelectorAll('.'+classe);
    console.log(thisChecked);
    if (!thisChecked) {
        for (i=0; i<cblist.length; i++) {
            if (cblist[i].checked == true){
                cblist[i].checked = false;
                togglePhon(cblist[i].id)
            }
        }
    } else {
        for (i=0; i<cblist.length; i++) {
            if (cblist[i].checked == false){
                cblist[i].checked = true;
                togglePhon(cblist[i].id)
            }
        }
    }
}

// SAME FOR ALL LANGUAGES :) DO IT ONCE
// GENERATION DES CHECKBOX+COLORPICKER
var bi = ["PREPDET", "PRONADV"]
makeCustom()
function makeCustom() {
    custphonList = document.querySelectorAll('.custcol');
    for (i=0; i<custphonList.length; i++) {
        var thispos = custphonList[i].dataset.pos;
        
        var colpick = ' <input type="color" id="colorPicker-'+thispos+'" class="colorSetting" onchange="setCol(\''+thispos+'\',this.value)" value="'+ pos[thispos].color +'">';
        if (bi.includes(thispos)){
            colpick = '';
        }

        var title = ""
        if (Object.keys(pos[thispos]).includes("help")) {
            if (Object.keys(pos[thispos].help).includes(thisPageLang)) {
                title = ` title="${pos[thispos].help[thisPageLang]}"`;
            }
        }

        custphonList[i].innerHTML = '<div class="form-check m-1"><input class="form-check-input check-pos" type="checkbox" onchange="togglePhon(this.id)" id="cb-'+thispos+'" checked><label class="form-check-label" for="cb-'+thispos+'" '+title+'>'+pos[thispos][thisPageLang]+colpick+'</label></div>';
    }
}