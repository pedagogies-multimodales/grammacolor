// https://universaldependencies.org/u/pos/index.html
var pos = {
    "ADJ": { "color":"#980086", "fr":"Adjectif", "en":"Adjective", "help": { "fr":"grand, belle, jolis, vert...", "en":"big, old, green, African, incomprehensible, first..."}},
    "ADV": { "color":"#3984c6", "fr":"Adverbe", "en":"Adverb", "help": { "fr":"très, bien, exactement, demain, ensemble", "en":"very, well, exactly, tomorrow, up, where, here..."}},
    "AUX": { "color":"#eda01a", "fr":"Auxiliaire", "en":"Auxiliary", "help": { "fr":"être/avoir/faire", "en":"has, is, will, was, got, should..."}},
    "CONJ": { "color":"#ffffff", "fr":"Conjonction", "en":"Conjunction", "help": { "fr":"", "en":""}},
    "CCONJ": { "color":"#ffffff", "fr":"Conjonction de coordination", "en":"Coordinating conjunction", "help": { "fr":"mais, où, et, donc, or, ni, car...", "en":"and, or, but..."}},
    "DET": { "color":"#f3eb20", "fr":"Déterminant", "en":"Determiner", "help": { "fr":"le, la, les, mon, cette, quelles, auncun...", "en":"the, a, an, three, many..."}},
    "INTJ": { "color":"#8a9092", "fr":"Interjection", "en":"Interjection", "help": { "fr":"bref, bon, enfin...", "en":"psst, ouch, bravo, hello..."}},
    "NOUN": { "color":"#00b54a", "fr":"Nom", "en":"Noun", "help": { "fr":"montagne, nuages, sushi, bateau...", "en":"girl, tree, sushi, decision..."}},
    "NUM": { "color":"#f3eb20", "fr":"Nombre", "en":"Numeral", "help": { "fr":"0, 1, 2, 11, soixante-dix-sept, IV...", "en":"0, 1, 2, 11, seventy-seven, K, IV..."}},
    "PART": { "color":"#cccccc", "fr":"Particule", "en":"Particle", "help": { "fr":"", "en":"'s, ', not, n't, to..."}},
    "ADP": { "color":"#c00018", "fr":"Préposition", "en":"Adposition", "help": { "fr":"pour, de, à, dans...", "en":"in, to, during..."}},
    "PREPDET": { "color":"#c00018", "fr":"Déterminant prépositionnel", "en":"", "help": { "fr":"du, des, au, aux...", "en":""}},
    "PRON": { "color":"#a56026", "fr":"Pronom", "en":"Pronoun", "help": { "fr":"je, tu, elle, ceux, me, qui...", "en":"I, you, she, myself, who, that, everybody, ours..."}},
    "PRONADV": {"color":"#a56026", "fr":"Pronom adverbial", "en":"", "help": { "fr":"en, y", "en":""}},
    "PROPN": { "color":"#00b54a", "fr":"Nom propre", "en":"Proper noun", "help": { "fr":"Pierre, ONU, Mexique...", "en":"Mary, John, London, NATO, HBO..."}},
    "PUNCT": { "color":"#ffffff", "fr":"Ponctuation", "en":"Punctuation", "help": { "fr":"point, virgule, parenthèses...", "en":"Period, comma, parentheses..."}},
    "SCONJ": { "color":"#ffffff", "fr":"Conjonction de subordination", "en":"Subordinating conjunction", "help": { "fr":"quand, parce que, avant que...", "en":"that, if, while..."}},
    "SPACE": { "color":"#cccccc", "fr":"Espace", "en":"Space", "help": { "fr":"", "en":""}},
    "SYM": { "color":"#cccccc", "fr":"Symbole", "en":"Symbol", "help": { "fr":"$, %, §, ©, ÷, =, <, :), ♥‿♥, 😝...", "en":"$, %, §, ©, ÷, =, <, :), ♥‿♥, 😝..."}},
    "VERB": { "color":"#eda01a", "fr":"Verbe", "en":"Verb", "help": { "fr":"manger, mangerons, vois, allions...", "en":"run, eat, runs, ate, running, eating..."}},
    "X": { "color":"#cccccc", "fr":"X", "en":"Other", "help": { "fr":"xxx, xfgh, jklw...", "en":"xxx, xfgh, jklw..."}},
    "X・": { "color":"#cccccc", "fr":"X", "en":"X", "help": { "fr":"", "en":""}},
    "col_base": { "color":"#555753", "fr":"", "en":"", "help": { "fr":"", "en":""}},
    
    
    "名詞・代名詞" : { "color":"#a56026", "jp":"名詞 代名詞 (pronom)" },
    "副詞・一般" : { "color":"#3984c6", "jp":"副詞 一般 (adverbe)" },
    "副詞・助詞類接続" : { "color":"#3984c6", "jp":"副詞 助詞類接続 (adverbe à particule)" },
    "名詞・副詞可能" : { "color":"#00b54a", "jp":"名詞 副詞可能 (nom adverbe)" },
    
    "感動詞・" : { "color":"#8a9092", "jp":"感動詞 (interjection)" },
    "その他・間投" : { "color":"#8a9092", "jp":"その他 間投 (autres interjections)" },
    "フィラー・" : { "color":"#8a9092", "jp":"フィラー (filler)" },
    

    "形容詞・自立" : { "color":"#980086", "jp":"形容詞 自立 (adjectif indépendant)" },
    "形容詞・非自立" : { "color":"#980086", "jp":"形容詞 非自立 (adjectif dépendant)" },
    "連体詞・" : { "color":"#980086", "jp":"連体詞 (adjectif prénominal)" },
    "形容詞・接尾" : { "color":"#980086", "jp":"形容詞 接尾 (suffixe adjectival)" },

    "連体詞・係助詞" : { "color":"#980086", "jp":"連体詞 係助詞" },
    "連体詞・副助詞／並立助詞／終助詞" : { "color":"#980086", "jp":"連体詞 副助詞／並立助詞／終助詞" },
    "連体詞・特殊" : { "color":"#980086", "jp":"連体詞 特殊" },
    "接頭詞・形容詞接続" : { "color":"#ffffff", "jp":"接頭詞 形容詞接続 (préfixe adjectival)" },
    
    "名詞・一般" : { "color":"#00b54a", "jp":"名詞 一般 (nom général)" },
    "名詞・固有名詞" : { "color":"#00b54a", "jp":"名詞 固有名詞 (nom propre)" },
    "名詞・引用文字列" : { "color":"#00b54a", "jp":"名詞 引用文字列 (nom de citation いわく)" },
    "名詞・動詞非自立的" : { "color":"#00b54a", "jp":"名詞 動詞非自立的 (nom verbe-dépendant)" },
    
    "名詞・非自立" : { "color":"#00b54a", "jp":"名詞 非自立 (nom dépendant)" },
    "名詞・形容動詞語幹" : { "color":"#00b54a", "jp":"名詞 形容動詞語幹 (nom adjectif)" },
    "名詞・接尾" : { "color":"#00b54a", "jp":"名詞 接尾 (suffixe nominal)" },
    "名詞・特殊" : { "color":"#00b54a", "jp":"名詞 特殊 (nom spécial)" },
    
    "名詞・数" : { "color":"#00b54a", "jp":"名詞 数 (nom numéral)" },
    "名詞・接続詞的" : { "color":"#00b54a", "jp":"名詞 接続詞的 (nom conjonctif「VS」、「対」、「兼」)" },
    "名詞・サ変接続" : { "color":"#00b54a", "jp":"名詞 サ変接続 (nom sa-hen)" },
    "名詞・ナイ形容詞語幹" : { "color":"#00b54a", "jp":"名詞 ナイ形容詞語幹 (nom en -nai)" },
    
    "接頭詞・名詞接続" : { "color":"#00b54a", "jp":"接頭詞 名詞接続 (préfixe nominal)" },
    "接頭詞・数接続" : { "color":"#00b54a", "jp":"接頭詞 数接続 (préfixe numérique)" },
    
    
    
    "助詞・副助詞" : { "color":"#f3eb20", "jp":"助詞 副助詞 (particule adverbe)" },
    "助詞・接続助詞" : { "color":"#f3eb20", "jp":"助詞 接続助詞 (particule conjonctive)" },
    "助詞・格助詞" : { "color":"#f3eb20", "jp":"助詞 格助詞 (particule de cas)" },
    "助詞・連体化" : { "color":"#f3eb20", "jp":"助詞 連体化 (particule adjectivale)" },
    
    "助詞・係助詞" : { "color":"#f3eb20", "jp":"助詞 係助詞 (particule connective)" },
    "助詞・副助詞／並立助詞／終助詞" : { "color":"#f3eb20", "jp":"助詞 副助詞／並立助詞／終助詞 (particule divers)" },
    "助詞・副詞化" : { "color":"#f3eb20", "jp":"助詞 副詞化 (particule adverbiale)" },
    "助詞・並立助詞" : { "color":"#f3eb20", "jp":"助詞 並立助詞 (parallel marker)" },

    "助詞・終助詞" : { "color":"#f3eb20", "jp":"助詞 終助詞 (particule de fin de phrase)" },
    "助動詞・" : { "color":"#eda01a", "jp":"助動詞 (particule auxiliaire)" },
    
    

    "動詞・自立" : { "color":"#eda01a", "jp":"動詞 自立 (racine verbale)" },
    "動詞・非自立" : { "color":"#eda01a", "jp":"動詞 非自立 (verbe dépendant)" },
    "接頭詞・動詞接続" : { "color":"#eda01a", "jp":"接頭詞 動詞接続 (préfixe verbal)" },
    "動詞・接尾" : { "color":"#eda01a", "jp":"動詞 接尾 (suffix verbal)" },

    "接続詞・" : { "color":"#ffffff", "jp":"接続詞 (conjonction)" },
    "接続詞・並立助詞" : { "color":"#ffffff", "jp":"接続詞 並立助詞 (conjonction parallèle)" },
    "接続詞・副詞化" : { "color":"#ffffff", "jp":"接続詞 副詞化 (conjonction adverbiale)" },
    "接続詞・終助詞" : { "color":"#ffffff", "jp":"接続詞 終助詞 (conjonction de fin de phrase)" },

    "記号・アルファベット" : { "color":"#cccccc", "jp":"記号 アルファベット (lettre latine)" },
    "記号・一般" : { "color":"#cccccc", "jp":"記号 一般 (symbole général)" },
    "記号・句点" : { "color":"#cccccc", "jp":"記号 句点 (point)" },
    "記号・括弧閉" : { "color":"#cccccc", "jp":"記号 括弧閉 (parenthèse fermante)" },

    "記号・括弧開" : { "color":"#cccccc", "jp":"記号 括弧開 (parenthèse ouvrante)" },
    "記号・空白" : { "color":"#cccccc", "jp":"記号 空白 (espace)" },
    "記号・読点" : { "color":"#cccccc", "jp":"記号 読点 (virgule)" },
    

    "PN": { "color":"#a56026", "zh":"Pronoun"},
    
    "AD": { "color":"#3984c6", "zh":"Adverb"},
    
    "IJ": { "color":"#8a9092", "zh":"Interjection"},
    "ON": { "color":"#8a9092", "zh":"Onomatopoeia"},    
    "VA": { "color":"#eda01a", "zh":"Predicative adjective"},
    


    "NN": { "color":"#00b54a", "zh":"Common noun"},
    "NR": { "color":"#00b54a", "zh":"Proper noun"},
    "VV": { "color":"#eda01a", "zh":"Verb"},
    "VC": { "color":"#eda01a", "zh":"是"},

    "NT": { "color":"#00b54a", "zh":"Temporal noun"},
    "FW": { "color":"#00b54a", "zh":"Foreign words"},
    "VE": { "color":"#eda01a", "zh":"有 as the main verb"},
    "AS": { "color":"#eda01a", "zh":"Aspect marker"},
    
    "DT": { "color":"#f3eb20", "zh":"Determiner"},
    "CD": { "color":"#f3eb20", "zh":"Cardinal number "},
    "OD": { "color":"#f3eb20", "zh":"Ordinal number "},
    "M": { "color":"#f3eb20", "zh":"Measure word"},
    
    "LC": { "color":"#c00018", "zh":"Localizer"},
    
    "CC": { "color":"#ffffff", "zh":"Coordinating conjunction"},
    "CS": { "color":"#ffffff", "zh":"Subordinating conjunction"},
    
    "P": { "color":"#c00018", "zh":"Preposition excl. 被 and 把"},
    "LB": { "color":"#c00018", "zh":"被 in long bei-const"},
    "SB": { "color":"#c00018", "zh":"被 in short bei-const"},
    "BA": { "color":"#c00018", "zh":"把 in ba-construction"},
    
    "DEC": { "color":"#c00018", "zh":"的 in a relative-clause"},
    "DER": { "color":"#c00018", "zh":"得 in V-de const. and V-de-R"},
    "DEG": { "color":"#c00018", "zh":"Associative 的"},
    "DEV": { "color":"#c00018", "zh":"地 before VP"},
    
    "SP": { "color":"#8a9092", "zh":"Sentence-final particle"},
    "MSP": { "color":"#8a9092", "zh":"Other particle"},
    "JJ": { "color":"#cccccc", "zh":"Other noun-modifier"},
    "ETC": { "color":"#3984c6", "zh":"For words 等, 等等"},

    "PU": { "color":"#cccccc", "zh":"Punctuation"}
}

// https://hayashibe.jp/tr/mecab/dictionary/ipadic
// https://www.unixuser.org/~euske/doc/postag/

// Xia2000_The Part-Of-Speech Tagging Guidelines for thePenn Chinese Treebank 3.0.pdf
