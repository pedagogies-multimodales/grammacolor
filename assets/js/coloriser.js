$(document).ready(function(){
	var txtarea = document.getElementById("inText");
	txtarea.selectionEnd = txtarea.innerHTML.length;
	txtarea.focus();    
});

async function getColorisation() {
	//document.getElementById('loader').style.display = "block";

	var inText = document.getElementById('inText').value;
	var lang = 'fr' //document.getElementById('choixLang').value;

	// ON EMBALLE TOUT ÇA
    var colis = {
		inText: inText,
		lang: lang
	}
	console.log('Envoi colis:',colis);

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	const response = await fetch('/runSpacy/', options)
	console.log('En attente de réponse...');
    const data = await response.json();
	console.log(data);
	
	
}